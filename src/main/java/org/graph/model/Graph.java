package org.graph.model;

import org.graph.exceptions.HasManyEdgesException;
import org.graph.exceptions.HasMaxVertexException;

import java.util.*;
import java.util.stream.Collectors;

/**
 * ���������� ����������������� ����
 */
public class Graph{
    private List<Edge> edgeList;
    private static final int MAX_WEIGHT = 10000;
    private final int MAX_VERTEX = 1000;

    public Graph() {
        edgeList = new ArrayList<>();
    }

    /**
     * �������� ����� �� ���������� ��������� � �������
     *
     * @param vCount ���������� ������
     * @param eCount ���������� �����
     * @throws HasMaxVertexException
     */
    public Graph(int vCount, int eCount) throws HasMaxVertexException, HasManyEdgesException {
        if(vCount > MAX_VERTEX )//���� � ����� ������ ��� 1000 ������, �� ����� ���� ������ ���������
            throw new HasMaxVertexException();

        if(eCount > vCount + 1)//���������, ���� � ����� ����� ������ ��� (���������� ������ + 1), �� ����� ���� ���������� ���������
            throw new HasManyEdgesException();

        edgeList = new ArrayList<>();
        LinkedList<Vertex> vertexMap = new LinkedList<>();

        // ������� ������ ������
        int num_of_vertex = 0;

        while (num_of_vertex < vCount) {
            Vertex v = new Vertex(String.valueOf(num_of_vertex));
            vertexMap.add(v);
            num_of_vertex++;
        }

        // ������� ���� �� ���������� ������ ������
        int num_of_edges = 0;
        int index = 0;
        edgeList = new ArrayList<>();
        while (!vertexMap.isEmpty()) {

            if (num_of_edges < eCount) {//������� �����
                // ����������� ��������� �������
                Vertex v1 = vertexMap.get((int) (Math.random() * (vCount)));
                Vertex v2 = vertexMap.get((int) (Math.random() * (vCount)));

                if (!hasEdge(v1, v2)) {
                    addEdge(v1, v2, (int) (Math.random() * MAX_WEIGHT));
                    num_of_edges++;
                }
            } else {//��������� ������� � ����, ������� �������� ��� �����
                Vertex v = vertexMap.pop();
                if (!hasEdge(v)) {
                    addVertex(v.getName());
                }
            }
        }
    }

    /**
     * ������� ����� � ����� ����� ���������
     *
     * @param name1  ��� ������ �������
     * @param name2  ��� ������ �������
     * @param weight ��� �����
     */
    public void addEdge(String name1, String name2, int weight) {
        Vertex v1 = new Vertex(name1);
        Vertex v2 = new Vertex(name2);
        edgeList.add(new Edge(v1, v2, weight));
    }

    public void addEdge(Vertex v1, Vertex v2, int weight){
        edgeList.add(new Edge(v1, v2, weight));
    }

    public void addVertex(String name) {
        Vertex v = new Vertex(name);
        edgeList.add(new Edge(v, null, 0));
    }

    public boolean hasEdge(Vertex v) {
        for (Edge e : edgeList) {
            if (e.getV2() != null) {
                if (e.getV1().equals(v) || e.getV2().equals(v)) {
                    return true;
                }
            }else
                return false;
        }
        return false;
    }

    public boolean hasEdge(Vertex v1, Vertex v2) {
        if (edgeList.contains(new Edge(v1, v2)) || edgeList.contains(new Edge(v2, v1)))
            return true;
        return false;
    }

    /**
     * �������� ������ ����� ����������� ������ �������
     *
     * @param name
     * @return
     */
    public List<Edge> getAdjacentEdge(String name) {
        List<Edge> edges = new ArrayList<>();
        for (Edge e : edgeList) {

            Vertex v1 = e.getV1();
            Vertex v2 = e.getV2();

            if (v1.getName().equals(name)) {
                edges.add(e);
            }

            if (v2 != null) {
                if (e.getV2().getName().equals(name))
                    edges.add(e);
            }

        }

        return edges;
    }

    public List<Edge> getEdgesList() {
        return edgeList.stream().filter(e -> e.getV2() != null).collect(Collectors.toList());
    }

    public void setVertexMap(ArrayList<Edge> vertexList) {
        this.edgeList = vertexList;
    }

    public String print() {
        String text = "";
        for (Edge e : edgeList) {
            Vertex v1 = e.getV1();
            Vertex v2 = e.getV2();
            if (v2 != null) {
                text += "\n" + v1+ " ������� � " + v2 + " (��� " + e.getWeight() + ")";
                text += "\n" + v2 + " ������� � " + v1 + " (��� " + e.getWeight() + ")";
                //System.out.printf("%s ������� � �������� %s (��� %s) \n", v1.getName(), v2.getName(), e.getWeight());
                //System.out.printf("%s ������� � �������� %s (��� %s) \n", v2.getName(), v1.getName(), e.getWeight());
            } else
                //System.out.printf("%s �� �������  \n", v1.getName());
                text += "\n" + v1 + " �� �������";
        }
        return  text;
    }

    /**
     * �������� ������ ������
     * @return
     */
    public List<Vertex> getVertexList() {
        List<Vertex> vertexList = new ArrayList<>();
        for (Edge e : edgeList) {
            Vertex v1 = e.getV1();
            Vertex v2 = e.getV2();

            if (!vertexList.contains(v1))
                vertexList.add(v1);
            if (!vertexList.contains(v2) && v2 != null)
                vertexList.add(v2);
        }

        return vertexList;
    }

    /**
     * �������� ������������� ������ �� ��������
     *
     * @param name
     * @return
     */
    public int getGradeOfVertex(String name) {
        int count = 0;

        Vertex v = new Vertex(name);

        for (Edge edge : edgeList) {
            if (edge.getV1().equals(v) && edge.getV2() != null)
                count++;
            if (edge.getV2() != null) {
                if (edge.getV2().equals(v))
                    count++;
            }

        }

        return count;
    }

    /**
     * �������, ������� ���������� ����� ����������� ����.
     * @return
     */
    public Vertex getMaxAdjacentVertex() {
        int max = 0;
        Vertex v = null;
        for (Vertex vertex : getVertexList()) {
            int val = getGradeOfVertex(vertex.getName());
            if(val > max) {
                max = val;
                v = vertex;
            }
        }

        return v;
    }

    /**z
     * @return �������� ����������� ������������� ���� �� ��������.
     */
    public List<Edge> getWeightsOfEdges() {
        Comparator<Edge> c = (s1, s2) -> s1.getWeight().compareTo(s2.getWeight());
        ArrayList<Edge> el = (ArrayList<Edge>) edgeList.stream().sorted(c).collect(Collectors.toList());
        return el;
    }


}
