package org.graph.model;

/**
 * �����
 */
public class Edge {
    private Integer weight;
    private Vertex v1, v2;

    public String getName() {
        if(v2 != null)
            return "����� \"" + v1.getName() + "," + v2.getName() + "\"";
        else
            return "����� \"" + v1.getName() + "\"";
    }

    private String name;
    public Vertex getV1() {
        return v1;
    }

    public void setV1(Vertex v1) {
        this.v1 = v1;
    }

    public Edge(Vertex v1, Vertex v2) {
        this.weight = 0;
        this.v1 = v1;
        this.v2 = v2;
    }

    public Edge(Vertex v1, Vertex v2, Integer weight) {
        this.weight = weight;
        this.v1 = v1;
        this.v2 = v2;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Vertex getV2() {
        return v2;
    }

    public void setV2(Vertex v2) {
        this.v2 = v2;
    }

    public void setVertex(Vertex v2, int weight) {
        this.v2 = v2;
        this.weight = weight;
    }



    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((v2 == null) ? 0 : v2.hashCode());
        result = prime * result + weight;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Edge other = (Edge) obj;

        if(v2 == null)
        {
            if(v1.equals(other.getV1()) && other.weight == 0)
                return  true;
            else
                return  false;
        }

        if (!v2.equals(other.getV2()) || !v1.equals(other.getV1()))
            return false;

        return true;
    }

    @Override
    public String toString() {
        if (v2 != null)
            return getName() + " = " + weight;
        else
            return getName() + " = " + weight;
    }
}
